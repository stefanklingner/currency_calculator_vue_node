
var should = require('chai').should();

var currencies = require('../js/currencies.js');

var foo = 'bar';


var rates = {
    "EUR" : {
        "EUR" : 1,
        "USD" : 1.2,
        "BTC" : 3000,
        "ETH": 300,
        "LTC" : 200,
        "IOTA" : 2.3
    },
    "USD" : {
        "EUR" : 0.8,
        "USD" : 1,
        "BTC" : 3200,
        "ETH": 320,
        "LTC" : 210,
        "IOTA" : 2.1
    },
    "BTC" : {
        "EUR" : 3333,
        "USD" : 3500,
        "BTC" : 1,
        "ETH": 320,
        "LTC" : 210,
        "IOTA" : 2.1
    },
    "ETH" : {
        "EUR" : 0.8,
        "USD" : 1,
        "BTC" : 3200,
        "ETH": 1,
        "LTC" : 210,
        "IOTA" : 2.1
    },
    "LTC" : {
        "EUR" : 0.8,
        "USD" : 180,
        "BTC" : 3200,
        "ETH": 320,
        "LTC" : 1,
        "IOTA" : 2.1
    },
    "IOTA" : {
        "EUR" : 100,
        "USD" : 112,
        "BTC" : 3200,
        "ETH": 320,
        "LTC" : 210,
        "IOTA" : 1
    }
};

    describe('String', function() {
        describe('foo', function() {
            it('should be a string', function() {
                foo.should.be.a('string');
            });
        });
    });


    describe('calculation', function() {

        describe('3 EUR', function() {
           it('should be 9000 BTC', function() {
               currencies.setRates(rates);
               var result = currencies.calculate('EUR', 3);

               (result['btc']).should.be.equal(9000);
           });
        });

        describe('setRates', function() {
            it('should work' , function() {
                    currencies.setRates(rates);
                    var result = currencies.getRates();

                (result['EUR']['USD']).should.be.equal(1.2);
            });
        });

        describe('getRatesFromProvider', function() {
            it('should work' , function() {

            });
        });


    });


