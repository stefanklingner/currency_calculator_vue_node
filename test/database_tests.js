
const should = require('chai').should();

var database = require('../js/database.js');


var rates = {
    "EUR" : {
        "EUR" : 1,
        "USD" : 1.2,
        "BTC" : 3000,
        "ETH": 300,
        "LTC" : 200,
        "IOTA" : 2.3
    },
    "USD" : {
        "EUR" : 0.8,
        "USD" : 1,
        "BTC" : 3200,
        "ETH": 320,
        "LTC" : 210,
        "IOTA" : 2.1
    },
    "BTC" : {
        "EUR" : 3333,
        "USD" : 3500,
        "BTC" : 1,
        "ETH": 320,
        "LTC" : 210,
        "IOTA" : 2.1
    },
    "ETH" : {
        "EUR" : 0.8,
        "USD" : 1,
        "BTC" : 3200,
        "ETH": 1,
        "LTC" : 210,
        "IOTA" : 2.1
    },
    "LTC" : {
        "EUR" : 0.8,
        "USD" : 180,
        "BTC" : 3200,
        "ETH": 320,
        "LTC" : 1,
        "IOTA" : 2.1
    },
    "IOTA" : {
        "EUR" : 100,
        "USD" : 112,
        "BTC" : 3200,
        "ETH": 320,
        "LTC" : 210,
        "IOTA" : 1
    }
};

describe('database', function() {

    describe('insert one', function() {
        it('should inserted one record', async function() {

            rates.date_time = new Date();

            var r = await database.insert_currency_rate(rates);
            r.should.not.equal(undefined);

        });
    });

});
