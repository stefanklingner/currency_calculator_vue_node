const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://127.0.0.1:27017';

// Database Name
const dbName = 'currency_calculator';

// Create a new MongoClient
const client = new MongoClient(url,{useNewUrlParser: true, useUnifiedTopology: true});


exports.insert_currency_rate = async function(currency_rate) {

    try {
        await client.connect();
        console.log("Connected correctly to server");

        const db = client.db(dbName);

        // Insert a single document
        let r = await db.collection('rates').insertOne(currency_rate);
        assert.equal(1, r.insertedCount);

        await client.close();

        return r;

    } catch (err) {
        console.log(err.stack);
    }

};
