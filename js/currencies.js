const https = require('https');
var database = require('./database.js');

var start = new Date();

var alreadyFetched = false;
var rates;

function getRatesFromProvider(setRatesFunc) {

    var getRequest = 'https://min-api.cryptocompare.com/data/pricemulti?fsyms=EUR,USD,BTC,ETH,LTC,MIOTA&tsyms=EUR,USD,BTC,ETH,LTC,MIOTA&extraParams=currency_calculator';

    var end = new Date() - start;
    if (!alreadyFetched || end > 10000) {

    https.get(getRequest, (res) => {
            console.log('statusCode:', res.statusCode);
            console.log('headers:', res.headers);

        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
            try {
                const parsedData = JSON.parse(rawData);
                setRatesFunc(parsedData);
                console.log(parsedData);
            } catch (e) {
                console.error(e.message);
            }
        });

        }).on('error', (e) => {
                console.log(e);
        });

        alreadyFetched = true;
        start = new Date();

    }

};

exports.calculate = function (currency, amount) {

    let rates = this.getRates();

    currency = currency.toUpperCase();

    let result = {

        "eur" : rates[currency]["EUR"] * amount,
        "usd" : rates[currency]["USD"] * amount,
        "btc" : rates[currency]["BTC"] * amount,
        "eth" : rates[currency]["ETH"] * amount,
        "ltc" : rates[currency]["LTC"] * amount,
        "miota" : rates[currency]["MIOTA"] * amount
    };

    return result;
};

exports.getRates = function() {
    return rates;
};

exports.setRates = function(newRates) {
    rates = newRates;

    // persist the rates async and for later use
    rates.date_time = new Date();
    database.insert_currency_rate(rates);

};

exports.fetchRates = function() {
    getRatesFromProvider(this.setRates);
};

