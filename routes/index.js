var express = require('express');
var router = express.Router();

var currencies = require('../js/currencies.js')


/* GET home page. */
router.get('/', function(req, res, next) {
  currencies.fetchRates();
  res.render('index');
});

router.get('/calculate', function (req, res) {

    currencies.fetchRates();

    let calculatedValues = currencies.calculate(req.query.currency, req.query.amount);

    console.log(calculatedValues);

    res.send(calculatedValues);
});

module.exports = router;
